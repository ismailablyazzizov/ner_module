import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

public class AppRunner {

    static String rulesFile = "rules/rules_cities_trc.txt";
    static String sourceFile = "source/aboutcities.txt";

    public static void main(String[] args) {
        System.out.println("Named entity recognition; run;");
        TokensRegexCities trc = new TokensRegexCities();
        trc.applyRulesFromFile(rulesFile,readTextFromFile(sourceFile));
        //trc.applyRulesFromFileTest(rulesFile);
    }

    static String readTextFromFile(String path){
        String contents = null;

        ClassLoader classLoader = ClassLoader.getSystemClassLoader();
        File file = new File(classLoader.getResource(path).getFile());
        try {
            contents = new String(Files.readAllBytes(file.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return contents;
    }

}
