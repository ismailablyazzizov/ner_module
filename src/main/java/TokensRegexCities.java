import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.*;
import edu.stanford.nlp.ling.tokensregex.types.Expressions;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

import java.awt.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Function;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summingInt;

public class TokensRegexCities {

    enum Attribute { CHARACTER, SIZE, CLIMATE, RESTORANTS, STYLE, ECONOMY }
    enum NecessaryTag { SIZE_BIG, SIZE_SMALL, POPULATION, CITY }

    private Map<String,String> features;
    private List<Opinion> opinions;

    public TokensRegexCities(){
        features = new HashMap<>();
        opinions = new ArrayList<>();
    }

    public void applyRulesFromFile(String rulesFile, String text){

        if (text.equals("")){
            System.out.println("No text to process(");
        }

        CoreMapExpressionExtractor<MatchedExpression> extractor = CoreMapExpressionExtractor
                .createExtractorFromFiles(TokenSequencePattern.getNewEnv(), rulesFile);

        StanfordCoreNLP pipeline = new StanfordCoreNLP(
                PropertiesUtils.asProperties("annotators", "tokenize,ssplit,pos,lemma"));

        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);

        int i = 0;
        for (CoreMap sentence : sentences) {
            System.out.println("Sentence #" + ++i);
            System.out.println(sentence.toString());
            //printTokensDetails(sentence);
            List<MatchedExpression> matchedExpressions = extractor.extractExpressions(sentence);
            System.out.println();

            for (MatchedExpression matched : matchedExpressions) {
                System.out.println("Matched expression: " /*+ matched.getText() */+ " : " + matched.getValue()+" >> ");

                //------custom processing------

                ArrayList<Expressions.PrimitiveValue> match_elements  = (ArrayList<Expressions.PrimitiveValue>)matched.getValue().get();
                String tag = match_elements.get(0).get().toString();
                System.out.println(tag);

                ArrayList<?> match_tokens = (ArrayList<?>)match_elements.get(1).get();
                StringBuilder builder = new StringBuilder();
                for (Object o : match_tokens) {
                    builder.append(o.toString().split("-")[0]+" ");
                }
                String value = builder.toString().trim();
                System.out.println(value);

                //if (contains(tag)){
                    opinions.add(new Opinion(tag,value));
               // }
            }
        }
        System.out.println("\n----------------------------------------< tags");

        for (Opinion o : opinions){
            if (o.getAttribute().equals("POPULATION")){
                    o.setValue(numberFromString(o.getValue()).toString());
            }
        }
        showOpinions();

        System.out.println("\n----------------------------------------< postprocessing");
        postprocess();
        cleanOpinions();
        //showOpinions();

        System.out.println("\n----------------------------------------< features (most frequent)");
        for (Map.Entry<String,String> entry : features.entrySet()){
            System.out.println(entry.getKey()+"->"+entry.getValue());
        }
    }

    private void postprocess(){
        ArrayList<Opinion> redefinedOpinions = new ArrayList<>();
        for (Opinion o : opinions){
            if (o.getAttribute().equals("POPULATION")){
                Double population = Double.valueOf(o.getValue());
                if(population<500000){
                    redefinedOpinions.add(new Opinion("SIZE_SMALL","someCity"));
                    System.out.println("!!!");
                } else{
                    redefinedOpinions.add(new Opinion("SIZE_BIG","someCity"));
                }

            }
        }
        opinions.addAll(redefinedOpinions);
    }

    private void showOpinions(){
        for (Opinion o : opinions){
            if (contains(o.getAttribute())){
                System.out.println(o.getAttribute()+"->"+o.getValue());
            }
        }
    }

    private void cleanOpinions(){
        Map<String, List<Opinion>> collect =
                opinions.stream().collect(groupingBy(Opinion::getAttribute));
        collect.forEach((k,v)->{
            System.out.print(k+"->");
            Map<String, Integer> values = new HashMap<>();
            for (Opinion o : v){
                if (!values.containsKey(o.getValue())){
                    values.put(o.getValue(),1);
                } else{
                    values.put(o.getValue(),values.get(o.getValue())+1);
                }
            }
            values.forEach((key,value)->{
                System.out.print(key+"["+value+"]  ");
            });
            System.out.println();

            features.put(k,values.entrySet().stream().max(Map.Entry.comparingByValue()).get().getKey());
        });
    }

    public void applyRulesFromFileTest(String rulesFile){
        String text = "With a U.S. Census Bureau-estimated 2015 population of 8,550,405 distributed over a land area of just 305 square miles (790 km2), New York is also the most densely populated major city in the United States. "+
                "Its estimated mid-2015 municipal population (corresponding to Greater London) was 8,673,713, the largest of any city in the European Union, and accounting for 12.5 per cent of the UK population"
                +" London's urban area is the second most populous in the EU, after Paris, with 9,787,426 inhabitants at the 2011 census."
                +"Amsterdam has a population of 842,343 within the city proper, 1,340,725 in the urban area, and 2,431,000 in the Amsterdam metropolitan area. "
                +"The population of the special wards is over 9 million people, with the total population of the prefecture exceeding 13 million. "
                +"Barcelona (/bɑːrsəˈloʊnə/, Catalan: [bəɾsəˈlonə], Spanish: [barθeˈlona]) is the capital city of the autonomous community of Catalonia in the Kingdom of Spain, "
                +"as well as the country's second most populous municipality, with a population of 1.6 million within city limits. Its urban area extends beyond the administrative city "
                +"limits with a population of around 4.7 million people, being the sixth-most populous urban area in the European Union after Paris, London, Madrid, the Ruhr area and Milan. "
                ;

        if (text.equals("")){
            System.out.println("No text to process(");
        }

        CoreMapExpressionExtractor<MatchedExpression> extractor = CoreMapExpressionExtractor
                .createExtractorFromFiles(TokenSequencePattern.getNewEnv(), rulesFile);

        StanfordCoreNLP pipeline = new StanfordCoreNLP(
                PropertiesUtils.asProperties("annotators", "tokenize,ssplit,pos,lemma"));

        Annotation annotation = new Annotation(text);
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);

        int i = 0;
        for (CoreMap sentence : sentences) {
            System.out.println("Sentence #" + ++i);
            System.out.println(sentence.toString());

            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                System.out.println("  Token: " + "word="+ token.get(CoreAnnotations.TextAnnotation.class) + ",  pos=" +
                        token.get(CoreAnnotations.PartOfSpeechAnnotation.class) + "" );
            }

            List<MatchedExpression> matchedExpressions = extractor.extractExpressions(sentence);
            System.out.println();

            for (MatchedExpression matched : matchedExpressions) {
                System.out.println("Matched expression: " /*+ matched.getText() */+ " : " + matched.getValue());
            }

        }
    }

    void printTokensDetails(CoreMap sentence){
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                System.out.println("  Token: " + "word="+ token.get(CoreAnnotations.TextAnnotation.class) + ",  pos=" +
                        token.get(CoreAnnotations.PartOfSpeechAnnotation.class) + "" );
            }
    }

    public boolean contains(String test) {

        for (NecessaryTag c : NecessaryTag.values()) {
            if (c.name().equals(test)) {
                return true;
            }
        }

        return false;
    }

    public Double numberFromString(String target){
        Double number = Double.valueOf(target.replace(",",""));
        if (number<50) {number=number*1000000;}
        return number;
    }
}
