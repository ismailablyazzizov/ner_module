import edu.stanford.nlp.ling.CoreAnnotations;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.ling.tokensregex.*;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.util.CoreMap;
import edu.stanford.nlp.util.PropertiesUtils;

import java.util.ArrayList;
import java.util.List;

public class TokensRegexTest {

    public TokensRegexTest(){

    }

    public void goTokens(){
        StanfordCoreNLP pipeline;
        pipeline = new StanfordCoreNLP(
                PropertiesUtils.asProperties("annotators", "tokenize,ssplit,pos,lemma"));
        Annotation annotation = new Annotation(

                "Casey was born in London."+
                        " Chicago Bulls will play next game in New York."+
                        " I lived in Berlin for 7 years."+
                        " Probably New York is a best city."+
                        " Though Rome is Italy’s much beloved capital."+
                        " London is the capital of GreatBritain."+
                        " Lisbon has long been one of the most underrated cities in Europe."

        );
        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);
        List<TokenSequencePattern> tokenSequencePatterns = new ArrayList<TokenSequencePattern>();
        String[] patterns = {
                        "(?$who [ {tag:/NNP.*/} | {tag:/PRP/} ]+ )  []*  (?$action [ { tag:/VB.*/ } ]+ ) []*  /in/ (?$location [ {tag:/NNP.*/}  ]+ ) ",

                        "(?$x [ {tag:/NNP.*/} ]+) [{tag:/VB.*/}] [{tag:/DT/}] [{tag:/JJ.*/}]* /city|town/",
                        "(?$location [ {tag:/NNP.*/} ]+) [{tag:/VB.*/}] [{tag:/DT/}]* [{tag:/JJ.*/}]* [ {tag:/NNP.*/} ]+/'s/ [{tag:/JJ.*/}]* /capital/",
                        "(?$location [ {tag:/NNP.*/} ]+) [{tag:/VB.*/}] [{tag:/DT/}]* [{tag:/JJ.*/}]* /capital/ /of/"
        };
        for (String line : patterns) {
            TokenSequencePattern pattern = TokenSequencePattern.compile(line);
            tokenSequencePatterns.add(pattern);
        }

        MultiPatternMatcher<CoreMap> multiMatcher = TokenSequencePattern.getMultiPatternMatcher(tokenSequencePatterns);
        int i = 0;
        for (CoreMap sentence : sentences) {
            List<CoreLabel> tokens = sentence.get(CoreAnnotations.TokensAnnotation.class);
            System.out.println("Sentence #" + ++i);
            System.out.print("  Tokens:");
            for (CoreLabel token : tokens) {
                System.out.print(' ');
                System.out.print(token.toShortString("Text", "PartOfSpeech", "NamedEntityTag"));
            }
            System.out.println();

            List<SequenceMatchResult<CoreMap>> answers = multiMatcher.findNonOverlapping(tokens);
            int j = 0;
            for (SequenceMatchResult<CoreMap> matched : answers) {
                System.out.println("  Match #" + ++j);

                System.out.println("    match: " + matched.group(0));
/*
                for (int k=1; k<matched.groupInfo().nodes.size()-1; k++){
                    System.out.println(k);
                    if (matched.groupInfo(k).varName != null){
                        System.out.println(matched.groupInfo(k).varName+": " + matched.group(matched.groupInfo(k).varName));
                    }
                }
*/
                System.out.println("      who: " + matched.group("$who"));
                //System.out.println("      age: " + matched.group("$age"));
                System.out.println("   action: " + matched.group("$action"));
                System.out.println(" location: " + matched.group("$location"));
            }
            if (j==0) {
                System.out.println("No matches found(");
            }
        }
    }

    public void goTokensOuterFile(){
        String rules = "rules/rules_cities_combined.txt";
        CoreMapExpressionExtractor<MatchedExpression> extractor = CoreMapExpressionExtractor
                .createExtractorFromFiles(TokenSequencePattern.getNewEnv(), rules);

        StanfordCoreNLP pipeline = new StanfordCoreNLP(
                PropertiesUtils.asProperties("annotators", "tokenize,ssplit,pos,lemma"));
        Annotation annotation = new Annotation(
               // "( ( five plus three plus four ) * 2 ) divided by three"
                "I lived in Chicago for 5 years, then moved to San Francisco."+
                        " Probably New York is a best city."+
                        " Though Rome is Italy’s much beloved capital."
        );

        pipeline.annotate(annotation);
        List<CoreMap> sentences = annotation.get(CoreAnnotations.SentencesAnnotation.class);

        int i = 0;
        for (CoreMap sentence : sentences) {
            System.out.println("Sentence #" + ++i);
            for (CoreLabel token : sentence.get(CoreAnnotations.TokensAnnotation.class)) {
                System.out.println("  Token: " + "word="+ token.get(CoreAnnotations.TextAnnotation.class) + ",  pos=" +
                        token.get(CoreAnnotations.PartOfSpeechAnnotation.class) + "" );
            }
            List<MatchedExpression> matchedExpressions = extractor.extractExpressions(sentence);
            System.out.println();

            for (MatchedExpression matched : matchedExpressions) {

                System.out.println("Matched expression: " + matched.getText() + " with value " + matched.getValue());
                /*
                CoreMap cm = matched.getAnnotation();
                System.out.println(""+matched.getText());
                for (CoreLabel token : cm.get(CoreAnnotations.TokensAnnotation.class)) {
                    String word = token.get(CoreAnnotations.TextAnnotation.class);
                    String lemma = token.get(CoreAnnotations.LemmaAnnotation.class);
                    String pos = token.get(CoreAnnotations.PartOfSpeechAnnotation.class);
                    System.out.println("  Matched token: " + "word="+word + ", lemma="+lemma + ", pos=" + pos + "");
                }
                */
            }

        }
    }

    private void smth(){
                /*
        TokenSequencePattern pattern = TokenSequencePattern.compile("/the/ /first/ /day/");
        TokenSequenceMatcher matcher = pattern.getMatcher(tokens);

        matcher.matches();
        matcher.find();

        String matched = matcher.group();
        List<CoreMap> matchedNodes = matcher.groupNodes();
        */

    }
}
